package com.eclipsekingdom.starmail;

import com.eclipsekingdom.starmail.box.*;
import com.eclipsekingdom.starmail.gui.InputListener;
import com.eclipsekingdom.starmail.gui.LiveSessions;
import com.eclipsekingdom.starmail.gui.page.PageType;
import com.eclipsekingdom.starmail.letter.Letter;
import com.eclipsekingdom.starmail.letter.LetterLoot;
import com.eclipsekingdom.starmail.pack.Pack;
import com.eclipsekingdom.starmail.pack.PackLoot;
import com.eclipsekingdom.starmail.pack.PackType;
import com.eclipsekingdom.starmail.pack.tracking.TrackingCache;
import com.eclipsekingdom.starmail.pack.tracking.TrackingRunnable;
import com.eclipsekingdom.starmail.post.CommandMail;
import com.eclipsekingdom.starmail.post.CommandSendTo;
import com.eclipsekingdom.starmail.post.PostCache;
import com.eclipsekingdom.starmail.postbox.Postbox;
import com.eclipsekingdom.starmail.postbox.PostboxCache;
import com.eclipsekingdom.starmail.postbox.PostboxLoot;
import com.eclipsekingdom.starmail.sys.Language;
import com.eclipsekingdom.starmail.sys.PluginBase;
import com.eclipsekingdom.starmail.sys.Version;
import com.eclipsekingdom.starmail.sys.config.ConfigLoader;
import com.eclipsekingdom.starmail.sys.config.IntegrationConfig;
import com.eclipsekingdom.starmail.sys.config.ItemsConfig;
import com.eclipsekingdom.starmail.sys.config.PluginConfig;
import com.eclipsekingdom.starmail.user.UserCache;
import com.eclipsekingdom.starmail.util.*;
import com.eclipsekingdom.starmail.warehouse.CommandWarehouse;
import com.eclipsekingdom.starmail.warehouse.WarehouseCache;
import org.bukkit.plugin.java.JavaPlugin;

public final class StarMail extends JavaPlugin {

    private static StarMail plugin;
    private static StarMailAPI starMailAPI = StarMailAPI.getInstance();

    @Override
    public void onEnable() {
        plugin = this;

        ConfigLoader.load();
        new PluginConfig();
        new IntegrationConfig();
        new ItemsConfig();
        Language.load();

        Box.init();
        Postbox.init();
        PackType.init();
        Pack.init();
        Letter.init();
        PageType.init();

        new PluginBase();

        new BoxCache();
        new PostboxCache();
        new UserCache();
        new PostCache();
        new WarehouseCache();

        new TrackingCache();
        TrackingRunnable.start();

        new LiveSessions();
        new Crafting();

        getCommand("boxes").setExecutor(new CommandBoxes());
        getCommand("breakboxes").setExecutor(new CommandBreakBoxes());
        getCommand("mail").setExecutor(new CommandMail());
        getCommand("sendto").setExecutor(new CommandSendTo());
        if (Version.current.hasLetter()) getCommand("letter").setExecutor(new CommandLoot(new LetterLoot()));
        getCommand("pack").setExecutor(new CommandLoot(new PackLoot()));
        getCommand("box").setExecutor(new CommandLoot(new BoxLoot()));
        getCommand("postbox").setExecutor(new CommandLoot(new PostboxLoot()));
        getCommand("globalbox").setExecutor(new CommandLoot(new GlobalLoot()));
        getCommand("starmail").setExecutor(new CommandStarMail());
        getCommand("warehouse").setExecutor(new CommandWarehouse());

        new InputListener();
        new ItemListener();
        new MailListener();
        if (Version.current.isAutoCompleteSupported()) new AutoCompleteListener();

    }

    @Override
    public void onDisable() {

        LiveSessions.shutdown();

        TrackingRunnable.shutdown();
        TrackingCache.shutdown();

        WarehouseCache.shutdown();
        PostCache.shutdown();
        UserCache.shutdown();
        PostboxCache.shutdown();
        BoxCache.shutdown();

        PluginBase.shutdown();
    }

    public static StarMail getPlugin() {
        return plugin;
    }

    public static StarMailAPI getStarMailAPI() {
        return starMailAPI;
    }

}
