package com.eclipsekingdom.starmail.gui.data;

import org.bukkit.inventory.Inventory;

public interface IUpdateable {

    void updateContents(Inventory menu);

}
